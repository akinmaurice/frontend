import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import { connect } from 'react-redux';

function mapStateToProps(state) {
    return {
      user: state.loginSuccess,
      authenticated: state.authenticated,
    };
  }



  class Header extends Component {
    componentDidMount() {
    }

    render() {
        let navLinks =  (
            <ul className="navbar-nav">
                <li className="nav-item">
                  <Link to="/login" className="nav-link__color">Sign In</Link> &nbsp;
                </li>
            </ul>
          );
          if (this.props.authenticated) {
            const { first_name } = this.props.user;
            navLinks = (
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link to="/main" className="nav-link__color">
                    {first_name}
                  </Link> &nbsp;
                </li>
                <li className="nav-item">
                  <Link to="/logout" className="nav-link__color">
                    Logout
                  </Link> &nbsp;
                </li>
              </ul>
            );
          }
          return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-inverse fixed-top">
            <div className="container-fluid">
              <Link to="/" className="navbar-brand py-0">Front End</Link>
              {navLinks}
            </div>
          </nav>
          )
    }
};

export default connect(mapStateToProps)(Header);
