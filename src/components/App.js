import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from './views/Header';


class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="jumbotron jumbotron-fluid jumbotron-started">
            <div className="container">
                <div className="row">
                    <div className="col-lg-6 jumbotron-head">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit

                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <p className="jumbotron-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        Ut enim ad minim veniam.
                        </p>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <Link to="/register" className="btn btn-lg btn-yellow btn-jumbotron">
                            GET STARTED &gt;
                        </Link>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                    <br/>
                    <br/>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="https://www.google.com" className="btn btn-primary btn-download-jumbotron">
                                    Apple
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.google.com" className="btn btn-primary btn-download-jumbotron">
                                    Android
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
  }
}

export default App;
