import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from '../views/Header';
import { loginUser, resetState } from '../../actions/index';


function mapStateToProps(state) {
    return {
      isError: state.isError,
      isLoading: state.loginIsLoading,
      errorMessage: state.errorMessage,
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
      fetchData: (payload) => dispatch(loginUser(payload)),
      resetData: () => dispatch(resetState()),
    };
  }


class Login extends Component {
  constructor() {
    super();
    this.state = {
        inputError: ''
      };
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillMount() {
    this.props.resetData();
  }


  onSubmit(event) {
    event.preventDefault();
    if(!this.email.value) {
        this.setState({
          inputError: 'Please provide a valid email address'
        })
        return;
      };
    if(!this.password.value) {
        this.setState({
          inputError: 'Please provide a password'
        })
        return;
    };
    if(this.email.value && this.password.value) {
        this.setState({
            inputError: ''
        })
    };
    const payload = {
      email: this.email.value,
      password: this.password.value,
    };
    this.props.fetchData(payload);
  }

  render() {
    const { errorMessage } = this.props;
    const { isError } = this.props;
    const { isLoading } = this.props;
    let view = '';
    let inputError = (<p></p>);
    if(this.state.inputError !== '') {
        inputError = (<p className="text-danger">{this.state.inputError}</p>)
      }
    if (isError) {
      view = errorMessage;
    } else if (isLoading) {
      view = <i className="fa fa-2x fa-circle-o-notch fa-spin" />;
    }
    return (
      <div>
      <Header />
        <div className="container text-center">
            <div className="row">
                <div className="col-lg-12">
                    <h4>
                        Sign In
                    </h4>
                </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <form className="form-signin" onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    ref={(input) => { this.email = input; }}
                    type="email"
                    name="email"
                    placeholder="Email Address"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.password = input; }}
                    type="password"
                    name="password"
                    placeholder="Password"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <button className="btn btn-green btn-lg btn-block">
                    Sign In
                  </button>
                </div>
                <div className="form-group form-link">
                  <Link to="/forgot-password">
                  Forgot Password?
                  </Link>
                </div>
                <div className="form-group form-link">
                  Don't have an account?&nbsp;
                  <Link to="/register">
                  Sign up
                  </Link>
                </div>
                <div className="form-group">
                 {inputError}
                </div>
                <div className="form-group">
                  <p className="text-danger">
                    {view}
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
