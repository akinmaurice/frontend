import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from '../views/Header';
import { registerUser, resetState } from '../../actions/index';


function mapStateToProps(state) {
    return {
      isError: state.isError,
      isLoading: state.loginIsLoading,
      errorMessage: state.errorMessage,
      registerSuccess: state.registerSuccess
    };
  }

  function mapDispatchToProps(dispatch) {
    return {
    fetchData: (payload) => dispatch(registerUser(payload)),
      resetData: () => dispatch(resetState()),
    };
  }



class Register extends Component {
  constructor() {
    super();
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
        inputError: ''
      };
  }

  componentWillMount() {
    this.props.resetData();
  }


  onSubmit(event) {
    event.preventDefault();
    if(!this.first_name.value) {
        this.setState({
          inputError: 'Please provide your First Name'
        })
        return;
      };
      if(!this.last_name.value) {
        this.setState({
          inputError: 'Please provide your Last Name'
        })
        return;
      };
    if(!this.email.value) {
        this.setState({
          inputError: 'Please provide a valid email address'
        })
        return;
      };
      if(!this.phone_number.value) {
        this.setState({
          inputError: 'Please provide a valid phone number'
        })
        return;
      };
    if(!this.password.value) {
        this.setState({
          inputError: 'Please provide a password'
        })
        return;
    };
    if(this.password.value !== this.confirmPassword.value) {
        this.setState({
          inputError: 'Passwords do not match'
        })
        return;
    };
    this.setState({
        inputError: ''
    });
    const payload = {
      first_name: this.first_name.value,
      last_name: this.last_name.value,
      email: this.email.value,
      phone_number: this.phone_number.value,
      password: this.password.value,
      confirmPassword: this.confirmPassword.value,
    };
    this.props.fetchData(payload);
  }

  render() {
    const { errorMessage, isError, isLoading, registerSuccess  } = this.props;
    let view = '';
    let inputError = (<p></p>);
    if(this.state.inputError !== '') {
        inputError = (<p className="text-danger">{this.state.inputError}</p>)
      }
    if (isError ) {
      view = errorMessage;
    } else if (isLoading) {
      view = <i className="fa fa-2x fa-circle-o-notch fa-spin text-danger" />;
    } else if(registerSuccess) {
      view = <Redirect to="/register/success" />;
    }
    return (
      <div>
          <Header />
            <div className="container text-center">
            <div className="row">
                <div className="col-lg-12">
                    <h4>
                        Create Account
                    </h4>
                </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <form className="form-signin" onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    ref={(input) => { this.first_name = input; }}
                    type="name"
                    name="first_name"
                    placeholder="First Name"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.last_name = input; }}
                    type="name"
                    name="last_name"
                    placeholder="Last Name"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.email = input; }}
                    type="email"
                    name="email"
                    placeholder="Email Address"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.phone_number = input; }}
                    type="text"
                    name="phone_number"
                    placeholder="Phone Number"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.password = input; }}
                    type="password"
                    name="password"
                    placeholder="Password"
                    className="form-control"
                  />
                </div>
                <div className="form-group">
                  <input
                    ref={(input) => { this.confirmPassword = input; }}
                    type="password"
                    name="confirmPassword"
                    placeholder="Confirm Password"
                    className="form-control"
                  />
                </div>
                <div className="form-group text-left">
                    <p className="form-agreement-text">
                        By clicking button below, you agree to <br /> our terms of acceptable use
                    </p>
                </div>
                <div className="form-group">
                  <button className="btn btn-green btn-lg btn-block">
                    Create Account
                  </button>
                </div>
                <div className="form-group text-danger">
                 {inputError}
                </div>
                <div className="form-group text-danger">
                  {view}
                </div>
                <div className="form-group form-link">
                  Have an account?&nbsp;
                  <Link to="/login">
                  Sign In
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (Register);
