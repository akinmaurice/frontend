import axios from 'axios';
import config from '../config';
import {
  IS_LOADING,
  IS_ERROR,
  ERROR_MESSAGE,
  AUTHENTICATED,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS
} from './types';

const baseApiUrl = `${config.baseApiUrl}/v1`;


export function authenticated(bool) {
    return {
      type: AUTHENTICATED,
      authenticated: bool,
  };
};


export const isLoading = (bool) => {
  return {
    type: IS_LOADING,
    isLoading: bool
  }
};


export const isError = (bool) => {
  return {
    type: IS_ERROR,
    isError: bool
  }
};


export const errorMessage = (errorMessage) => {
  return {
    type: ERROR_MESSAGE,
    errorMessage
  }
};


export const loginSuccess = (user) => {
    return {
        type: LOGIN_SUCCESS,
        user
    }
};


export const registerSuccess = (bool) => {
    return {
        type: REGISTER_SUCCESS,
        registerSuccess: bool
    }
};



export  const loginUser = (payload) => {
return async(dispatch) => {
    dispatch(isLoading(true));
    try {
    const response = await axios.post(`${baseApiUrl}/user/login`, payload);
    const { data: { data: { login_data } } } = response;
    const { user, token } = login_data;
    localStorage.setItem('token',token);
    dispatch(loginSuccess(user));
    dispatch(authenticated(true));
    } catch(e) {
        if(e.response && e.response.data){
        dispatch(isError(true));
        dispatch(errorMessage(e.response.data.message));
        } else {
            dispatch(isError(true));
            dispatch(errorMessage('Something went wrong'));
        }
    }
  }
};


export  const registerUser = (payload) => {
    return async(dispatch) => {
        dispatch(isLoading(true));
        try {
        await axios.post(`${baseApiUrl}/user/register`, payload);
        dispatch(registerSuccess(true));
        } catch(e) {
            if(e.response && e.response.data){
            dispatch(isError(true));
            dispatch(errorMessage(e.response.data.message));
            } else {
                dispatch(isError(true));
                dispatch(errorMessage('Something went wrong'));
            }
        }
    }
};



export default function logoutUser() {
    localStorage.clear();
    return (dispatch) => {
      dispatch(isError(false));
      dispatch(errorMessage(null));
      dispatch(loginSuccess({}));
      dispatch(authenticated(false));
    };
  };


export function resetState() {
  return (dispatch) => {
    dispatch(isError(false));
    dispatch(isLoading(false));
    dispatch(errorMessage(null));
  };
};


