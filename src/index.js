import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import WebFont from 'webfontloader';

import { store, persistor } from './store';


import App from './components/App';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import RegisterSuccess from './components/utils/RegisterSuccess';
import Logout from './components/auth/Logout';
import ForgotPassword from './components/auth/ForgotPassword';
import Main from './components/Main';
import NotFound from './components/NotFound';
import * as serviceWorker from './serviceWorker';



// import authenticated action from  Login action
import { authenticated } from './actions';

// Import HOC MiddleWares
import requireAuth from './components/hoc/require_auth';
import noRequireAuth from './components/hoc/no_require_auth';


import './css/bootstrap.min.css';
import './css/font-awesome.min.css';
import './css/style.css';

WebFont.load({
    google: {
      families: ['Montserrat:300,400,700', 'sans-serif', 'Open Sans', 'Nunito']
    }
  });


const user = localStorage.getItem('token');

if (user) {
  store.dispatch(authenticated(true));
}


const Root = () => (

  <Provider store={store}>
  <PersistGate persistor={persistor}>
    <Router>
      <div>
        <Switch>
          <Route path="/" exact component={App} />
          <Route path="/main" exact component={requireAuth(Main)} />
          <Route path="/login" exact component={noRequireAuth(Login)} />
          <Route path="/register" exact component={noRequireAuth(Register)} />
          <Route path="/register/success" exact component={noRequireAuth(RegisterSuccess)} />
          <Route path="/forgot-password" exact component={noRequireAuth(ForgotPassword)} />
          <Route path="/logout" exact component={requireAuth(Logout)} />
          <Route component={NotFound} />
        </Switch>
      </div>
    </Router>
    </PersistGate>
  </Provider>
  );



  render(<Root />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
