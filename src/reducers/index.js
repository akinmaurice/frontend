import { combineReducers } from 'redux';

import { isError, isLoading, errorMessage} from './utils';
import { loginSuccess, authenticated, registerSuccess } from './auth';

export default combineReducers({
  isError,
  isLoading,
  errorMessage,
  loginSuccess,
  authenticated,
  registerSuccess
});
